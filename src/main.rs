mod input;

use colored::Colorize;
use input::input;
use std::env;
use std::process::Command;
use std::io::Read;
use std::fs::File;

fn main() {
    let cmd_line: Vec<String> = env::args().collect();

    if cmd_line.len() != 2 {
        println!("Please use this command with 1 argument! The file name with the list of programs!");
    } else {
        let pkg_manager = input("What package manager do you use? (apt, pacman, pamac, yay, dnf, flatpak):".yellow().to_string().as_str(), true).to_lowercase();
        let proceed: bool;

        if pkg_manager == "apt" || pkg_manager == "dnf" || pkg_manager == "pacman" || pkg_manager == "pamac" || pkg_manager == "yay" || pkg_manager == "flatpak" {
            println!("{}", "That is a valid package manager!".green());
            proceed = true;
        } else {
            println!("{}", "That is not a valid package manager!".red());
            proceed = false;
        }

        if proceed == true {
            let exists = match File::open(cmd_line[1].to_string()) {
                Ok(_o) => true,
                Err(_e) => false,
            };

            if exists == false {
                println!("{}", "That file does not exist!".red());
            } else {
                let mut file = File::open(cmd_line[1].to_string()).unwrap();
                let mut data_raw = String::new();
                file.read_to_string(&mut data_raw).unwrap();
                let mut data: Vec<String> = Vec::new();
                for i in data_raw.trim().split("\n") {
                    data.push(i.to_string());
                }

                println!("{}", "Please enter your password.".yellow());
                Command::new("bash").args(["-c", "sudo"]).output().unwrap();

                for i in data.iter() {
                    println!("{} {}", "Installing...".blue().italic(), i.cyan().bold());
                    
                    if pkg_manager == "apt" {
                        Command::new("bash").args(["-c", format!("sudo apt install {} -y", i).as_str()]).output().unwrap();
                    }

                    else if pkg_manager == "dnf" {
                        Command::new("bash").args(["-c", format!("sudo dnf install {} --assumeyes", i).as_str()]).output().unwrap();
                    }

                    else if pkg_manager == "pacman" {
                        Command::new("bash").args(["-c", format!("sudo pacman -S {} --noconfirm", i).as_str()]).output().unwrap();
                    }

                    else if pkg_manager == "yay" {
                        Command::new("bash").args(["-c", format!("yay -S {} --noconfirm", i).as_str()]).output().unwrap();
                    }

                    else if pkg_manager == "pamac" {
                        Command::new("bash").args(["-c", format!("sudo pamac install {} --no-confirm", i).as_str()]).output().unwrap();
                    }

                    else if pkg_manager == "flatpak" {
                        Command::new("bash").args(["-c", format!("flatpak install {}", i).as_str()]).status().unwrap();
                    }
                }
            }
        }
    }
}