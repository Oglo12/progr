# progr
Give this program a list of programs to install, and it will install them for you. Works with most popular Linux package managers.

# Installing to /usr/bin/
> Requires: Git, Cargo
```
git clone https://codeberg.org/Oglo12/progr.git && cd progr && cargo build --release && sudo mv target/release/progr /usr/bin/ && cd .. && rm -rf progr && echo "" && echo "Done! You can now use the command 'progr'! :-D"
```
